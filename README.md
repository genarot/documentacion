# Curso Docker para integración continua
**Facilitador:** Juan Evangelista Fletes García.
## Recursos

 - [Instalación e introducción](https://gitlab.com/curso_docker/documentacion/blob/master/introduccion_instalacion.pptx)
 - [Docker Compose](https://gitlab.com/curso_docker/documentacion/blob/master/docker-compose.pptx)
 - [Imágenes](https://gitlab.com/curso_docker/documentacion/blob/master/imagenes.pptx)

## Evaluaciones

 - [I Prueba Evaluativa (20pts)](https://goo.gl/forms/4QwSkW2iMrOPRsHN2)
 - II Prueba Evaluativa (20pts)
 - Proyecto Final (60pts)
    - Email para enviar el proyecto final: curso.docker.ci@gmail.com
    - Fecha máxima de entrega: **20/01/2019**
	1.  **Crear un docker-compose.yml para una aplicación wordpress tomando en cuenta lo siguiente**:
		-   Utilizar imágenes oficiales    
		-   Debe de contar con dos servicios db y wordpress    
		-   Utilizar sustitución de variables para los datos de conexión en ambos servicios    
		-   Declarar la red    
		-   Versión de wordpress 5.0.2    
		-   Para db la imagen mysql:5.7
		-   El servicio de wordpress depende de db    
		-   El servicio de wordpress debe de correr en el puerto 8000    
		-   Agregar un healthcheck para wordpress    
		-   Dos volúmenes para el servicio wordpress uno themes y otro plugins(ver documentación de la imagen)    
		-   Un volumen para los datos de db    
		-   Para servicio wordpress politica de reinicio on-failure    
		-   Para servicio db política de reinicio always

	2. **Creación de un Dockerfile**
	    -   Utilizar imágenes oficiales
	    -   Utilizar imagen Ubuntu:16.04
		-   Crear una carpeta que se llame proyecto    
		-   Crear un fichero de texto llamado readme.txt y escribir su nombre completo    
		-   Crear un Dockerfile    
		-   Agregar labels    
		-   Instalar git    
		-   crear carpeta en ruta /data    
		-   copiar archivo a /data    
		-   exponer volumen /data    
		-   crear variable de entorno llamada HOLA con el siguiente valor HOLA DESDE MI CONTENEDOR    
		-   crear un cmd que imprima el valor de la variable HOLA

